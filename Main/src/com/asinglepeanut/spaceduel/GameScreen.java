package com.asinglepeanut.spaceduel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Author: Jonathan McDill
 * Date: 11/14/12
 * Time: 10:43 AM
 */
public class GameScreen implements Screen {

    World world;
    private Box2DDebugRenderer debugRenderer;
    private ShapeRenderer starRenderer;
    private SpriteBatch spriteBatch;
    private OrthographicCamera camera;
    private Ship ship1, ship2;
    private Stars stars;
    private Assets assets;
    private static final float CAMERA_WIDTH = 42;
    private static final float CAMERA_HEIGHT = 28;
    private Sprite ship1Sprite;
    private Sprite ship2Sprite;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        //SHIP 1
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            ship1.getBody().applyTorque(20);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            ship1.getBody().applyTorque(-20);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            ship1.getBody().applyForce(new Vector2((float) Math.cos(ship1.getBody().getAngle())*20, (float) Math.sin(ship1.getBody().getAngle())*20), ship1.getBody().getWorldPoint(new Vector2(0f, 0f)));

        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            ship1.getBody().setLinearDamping(3);
        } else ship1.getBody().setLinearDamping(0);

            //SHIP 2
        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
            ship2.getBody().applyAngularImpulse(0.01f);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.D)) {
            ship2.getBody().applyAngularImpulse(-0.01f);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.W)) {
            ship2.getBody().applyLinearImpulse(new Vector2((float)Math.cos(ship2.getBody().getAngle())*0.03f, (float)Math.sin(ship2.getBody().getAngle())*0.03f), ship2.getBody().getWorldCenter());
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)) {
            ship2.getBody().setLinearDamping(3);
        } else ship2.getBody().setLinearDamping(0);


        starRenderer.setProjectionMatrix(camera.combined);
        starRenderer.begin(ShapeRenderer.ShapeType.Point);
        starRenderer.setColor(Color.WHITE);
        for(Vector2 star : stars.getStars()){
            starRenderer.point(star.x, star.y, 0);
        }
        starRenderer.end();

        float spritePosX = ship1.getBody().getPosition().x * ppuX - ship1Sprite.getWidth();
        float spritePosY = ship1.getBody().getPosition().y * ppuY - ship1Sprite.getHeight();
        float spriteAngle = (float)Math.toDegrees(ship1.getBody().getAngle()) - 90;
        ship1Sprite.setPosition(spritePosX, spritePosY);
        ship1Sprite.setRotation(spriteAngle);

        spritePosX = ship2.getBody().getPosition().x * ppuX - ship1Sprite.getWidth();
        spritePosY = ship2.getBody().getPosition().y * ppuY - ship1Sprite.getHeight();
        spriteAngle = (float)Math.toDegrees(ship2.getBody().getAngle()) - 90;
        ship2Sprite.setPosition(spritePosX, spritePosY);
        ship2Sprite.setRotation(spriteAngle);

        spriteBatch.begin();
            ship1Sprite.draw(spriteBatch);
            ship2Sprite.draw(spriteBatch);
        spriteBatch.end();
        debugRenderer.render(world, camera.combined);
        camera.update();
        world.step(1 / 60f, 8, 3);
    }

    @Override
    public void resize(int width, int height) {
        setSize(width, height);
    }

    private int width;
    private int height;
    private float ppuX;	// pixels per unit on the X axis
    private float ppuY;	// pixels per unit on the Y axis
    public void setSize (int w, int h) {
        this.width = w;
        this.height = h;
        ppuX = (float)width / CAMERA_WIDTH;
        ppuY = (float)height / CAMERA_HEIGHT;
    }

    @Override
    public void show() {
        world = new World(new Vector2(0, 0), true);
        debugRenderer = new Box2DDebugRenderer();
        camera = new OrthographicCamera(44, 28);
        camera.position.set(44/2, 28/2, 0);
        starRenderer = new ShapeRenderer();
        stars = new Stars(2000, camera.viewportWidth, camera.viewportHeight);
        assets = new Assets();
        assets.load();
        spriteBatch =  new SpriteBatch();
        ship1Sprite = new Sprite(assets.ship2Texture,0,0, assets.ship2Texture.getWidth(), assets.ship2Texture.getHeight());
        ship2Sprite = new Sprite(assets.ship1Texture,0,0, assets.ship1Texture.getWidth(), assets.ship1Texture.getHeight());

        ship1 = new Ship(world, assets.ship2Texture.getWidth()*0.03f, assets.ship2Texture.getHeight()*0.03f);
        ship2 = new Ship(world, assets.ship1Texture.getHeight()*0.03f, assets.ship1Texture.getWidth()*0.03f);


    }


    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
