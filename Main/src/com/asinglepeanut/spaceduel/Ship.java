package com.asinglepeanut.spaceduel;

import com.badlogic.gdx.physics.box2d.*;

/**
 * Author: Jonathan McDill
 * Date: 11/14/12
 * Time: 3:47 PM
 */
public class Ship {


    public BodyDef getBodyDef() {
        return bodyDef;
    }

    private BodyDef bodyDef;

    public Body getBody() {
        return body;
    }

    public PolygonShape getShape() {
        return shape;
    }

    public FixtureDef getFixtureDef() {
        return fixtureDef;
    }

    private Body body;
    private PolygonShape shape;
    private FixtureDef fixtureDef;


    public Ship(World world, float shapex, float shapey){
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(20, 20);
        body = world.createBody(bodyDef);
        shape = new PolygonShape();
        shape.setAsBox(shapex,shapey);
        fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        fixtureDef.friction = 1f;
        fixtureDef.restitution = 1f;
        body.createFixture(fixtureDef);
        shape.dispose();
        body.setAngularDamping(3);
    }

    public void setPosition(float x, float y){
        bodyDef.position.set(x, y);
    }
}
