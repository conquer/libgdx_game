package com.asinglepeanut.spaceduel;

import com.badlogic.gdx.Game;

/**
 * User: Jonathan McDill
 * Date: 11/8/12
 * Time: 4:14 PM
 */
public class SpaceWars extends Game {


    @Override
    public void create() {
        setScreen(new GameScreen());
    }

}
