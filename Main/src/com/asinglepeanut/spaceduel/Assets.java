package com.asinglepeanut.spaceduel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Author: Jonathan McDill
 * Date: 11/14/12
 * Time: 12:39 PM
 */
public class Assets {
    public Texture ship1Texture;
    public Texture ship2Texture;

    public void load () {
        ship1Texture = new Texture(Gdx.files.internal("Owl.png"));
        ship2Texture = new Texture(Gdx.files.internal("Kestrel.png"));
    }
}
