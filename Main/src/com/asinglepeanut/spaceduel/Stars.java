package com.asinglepeanut.spaceduel;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Author: Jonathan McDill
 * Date: 11/14/12
 * Time: 11:20 AM
 */
public class Stars {

    private Array<Vector2> stars = new Array<Vector2>();
    Random rand = new Random();

    public Stars(int num, float width, float height){
        rand.setSeed(1337);
        for(int i = 0; i < num; i++) {
           stars.add(
                       new Vector2(rand.nextFloat()*width, rand.nextFloat()*height)
                    );
        }

    }

    public Array<Vector2> getStars() {
        return stars;
    }

}
