package com.asinglepeanut.libgdxgame;

import com.asinglepeanut.spaceduel.SpaceWars;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Author: Jonathan McDill
 * Date: 11/8/12
 * Time: 4:21 PM
 */
public class DesktopStarter {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Space Wars";
        cfg.useGL20 = false;
        cfg.width = 800;
        cfg.height = 480;
        new LwjglApplication(new SpaceWars(), cfg);
    }
}
